package com.ruoyi.app.controller;

import com.ruoyi.cms.service.ISysSiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestffController {

    @Autowired
    ISysSiteService sysSiteService;

    @RequestMapping("/test")
    @ResponseBody
    public String test() {

        return "test";
    }

}
