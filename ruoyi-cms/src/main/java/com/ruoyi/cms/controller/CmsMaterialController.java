package com.ruoyi.cms.controller;

import com.ruoyi.cms.domain.SysSite;
import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.cms.domain.CmsMaterial;
import com.ruoyi.cms.service.ICmsMaterialService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 素材Controller
 * 
 * @author ruoyi
 * @date 2020-09-10
 */
@Controller
@RequestMapping("/cms/material")
public class CmsMaterialController extends BaseController
{
    private String prefix = "cms/material";

    @Autowired
    private ICmsMaterialService cmsMaterialService;

    @RequiresPermissions("cms:material:view")
    @GetMapping()
    public String material()
    {
        return prefix + "/material";
    }

    /**
     * 查询素材列表
     */
    @RequiresPermissions("cms:material:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CmsMaterial cmsMaterial)
    {
        startPage();
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsMaterial.getSiteId() == null) {
                cmsMaterial.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsMaterial.getSiteId()) {
                return errorDataTable("站点权限不够");
            }
        }
        List<CmsMaterial> list = cmsMaterialService.selectCmsMaterialList(cmsMaterial);
        return getDataTable(list);
    }

    /**
     * 导出素材列表
     */
    @RequiresPermissions("cms:material:export")
    @Log(title = "素材", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CmsMaterial cmsMaterial)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsMaterial.getSiteId() == null) {
                cmsMaterial.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsMaterial.getSiteId()) {
                return AjaxResult.error("站点权限不够");
            }
        }
        List<CmsMaterial> list = cmsMaterialService.selectCmsMaterialList(cmsMaterial);
        ExcelUtil<CmsMaterial> util = new ExcelUtil<CmsMaterial>(CmsMaterial.class);
        return util.exportExcel(list, "material");
    }

    /**
     * 新增素材
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存素材
     */
    @RequiresPermissions("cms:material:add")
    @Log(title = "素材", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CmsMaterial cmsMaterial)
    {   SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsMaterial.getSiteId() == null) {
                cmsMaterial.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsMaterial.getSiteId()) {
                return AjaxResult.error("站点权限不够");
            }
        }
        return toAjax(cmsMaterialService.insertCmsMaterial(cmsMaterial));
    }

    /**
     * 修改素材
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        CmsMaterial cmsMaterial = cmsMaterialService.selectCmsMaterialById(id, sysSite.getId());
        mmap.put("cmsMaterial", cmsMaterial);
        return prefix + "/edit";
    }

    /**
     * 修改保存素材
     */
    @RequiresPermissions("cms:material:edit")
    @Log(title = "素材", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CmsMaterial cmsMaterial)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsMaterial.getSiteId() == null) {
                cmsMaterial.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsMaterial.getSiteId()) {
                return AjaxResult.error("站点权限不够");
            }
        }
        return toAjax(cmsMaterialService.updateCmsMaterial(cmsMaterial));
    }

    /**
     * 删除素材
     */
    @RequiresPermissions("cms:material:remove")
    @Log(title = "素材", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        return toAjax(cmsMaterialService.deleteCmsMaterialByIds(ids, sysSite.getId()));
    }
}
