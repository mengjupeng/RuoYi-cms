package com.ruoyi.cms.controller;

import java.util.List;

import com.ruoyi.cms.domain.SysSite;
import com.ruoyi.common.core.domain.Ztree;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.cms.domain.SysTheme;
import com.ruoyi.cms.service.ISysThemeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 模板主题Controller
 * 
 * @author ruoyi
 * @date 2020-09-12
 */
@Controller
@RequestMapping("/system/theme")
public class SysThemeController extends BaseController
{
    private String prefix = "system/theme";

    @Autowired
    private ISysThemeService sysThemeService;

    @RequiresPermissions("system:theme:view")
    @GetMapping()
    public String theme()
    {
        return prefix + "/theme";
    }

    /**
     * 查询模板主题列表
     */
    @RequiresPermissions("system:theme:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysTheme sysTheme)
    {
        startPage();
        List<SysTheme> list = sysThemeService.selectSysThemeList(sysTheme);
        return getDataTable(list);
    }

    /**
     * 导出模板主题列表
     */
    @RequiresPermissions("system:theme:export")
    @Log(title = "模板主题", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysTheme sysTheme)
    {
        List<SysTheme> list = sysThemeService.selectSysThemeList(sysTheme);
        ExcelUtil<SysTheme> util = new ExcelUtil<SysTheme>(SysTheme.class);
        return util.exportExcel(list, "theme");
    }

    /**
     * 新增模板主题
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存模板主题
     */
    @RequiresPermissions("system:theme:add")
    @Log(title = "模板主题", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysTheme sysTheme)
    {
        return toAjax(sysThemeService.insertSysTheme(sysTheme));
    }

    /**
     * 修改模板主题
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SysTheme sysTheme = sysThemeService.selectSysThemeById(id);
        mmap.put("sysTheme", sysTheme);
        return prefix + "/edit";
    }

    /**
     * 修改保存模板主题
     */
    @RequiresPermissions("system:theme:edit")
    @Log(title = "模板主题", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysTheme sysTheme)
    {
        return toAjax(sysThemeService.updateSysTheme(sysTheme));
    }

    /**
     * 删除模板主题
     */
    @RequiresPermissions("system:theme:remove")
    @Log(title = "模板主题", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(sysThemeService.deleteSysThemeByIds(ids));
    }

    /**
     * 选择模板主题管理树
     */
    @GetMapping(value = { "/selectThemeTree/" })
    public String selectThemeTree(ModelMap mmap)
    {
        return prefix + "/tree";
    }

    /**
     * 加载模板主题列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = sysThemeService.selectTree();
        return ztrees;
    }
}
