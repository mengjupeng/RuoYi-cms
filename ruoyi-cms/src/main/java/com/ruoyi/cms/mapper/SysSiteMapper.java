package com.ruoyi.cms.mapper;

import java.util.List;
import com.ruoyi.cms.domain.SysSite;

/**
 * 站点Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-06
 */
public interface SysSiteMapper 
{
    /**
     * 查询站点
     * 
     * @param id 站点ID
     * @return 站点
     */
    public SysSite selectSysSiteById(Long id);

    /**
     * 查询站点列表
     * 
     * @param sysSite 站点
     * @return 站点集合
     */
    public List<SysSite> selectSysSiteList(SysSite sysSite);

    /**
     * 新增站点
     * 
     * @param sysSite 站点
     * @return 结果
     */
    public int insertSysSite(SysSite sysSite);

    /**
     * 修改站点
     * 
     * @param sysSite 站点
     * @return 结果
     */
    public int updateSysSite(SysSite sysSite);

    /**
     * 删除站点
     * 
     * @param id 站点ID
     * @return 结果
     */
    public int deleteSysSiteById(Long id);

    /**
     * 批量删除站点
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysSiteByIds(String[] ids);

    SysSite selectBySitePath(String sitePath);
}
