package com.ruoyi.cms.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.cms.domain.SysSite;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.cms.mapper.SysThemeMapper;
import com.ruoyi.cms.domain.SysTheme;
import com.ruoyi.cms.service.ISysThemeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 模板主题Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-12
 */
@Service
public class SysThemeServiceImpl implements ISysThemeService 
{
    @Autowired
    private SysThemeMapper sysThemeMapper;

    /**
     * 查询模板主题
     * 
     * @param id 模板主题ID
     * @return 模板主题
     */
    @Override
    public SysTheme selectSysThemeById(Long id)
    {
        return sysThemeMapper.selectSysThemeById(id);
    }

    /**
     * 查询模板主题列表
     * 
     * @param sysTheme 模板主题
     * @return 模板主题
     */
    @Override
    public List<SysTheme> selectSysThemeList(SysTheme sysTheme)
    {
        return sysThemeMapper.selectSysThemeList(sysTheme);
    }

    /**
     * 新增模板主题
     * 
     * @param sysTheme 模板主题
     * @return 结果
     */
    @Override
    public int insertSysTheme(SysTheme sysTheme)
    {
        sysTheme.setCreateTime(DateUtils.getNowDate());
        return sysThemeMapper.insertSysTheme(sysTheme);
    }

    /**
     * 修改模板主题
     * 
     * @param sysTheme 模板主题
     * @return 结果
     */
    @Override
    public int updateSysTheme(SysTheme sysTheme)
    {
        sysTheme.setUpdateTime(DateUtils.getNowDate());
        return sysThemeMapper.updateSysTheme(sysTheme);
    }

    /**
     * 删除模板主题对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSysThemeByIds(String ids)
    {
        return sysThemeMapper.deleteSysThemeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除模板主题信息
     * 
     * @param id 模板主题ID
     * @return 结果
     */
    @Override
    public int deleteSysThemeById(Long id)
    {
        return sysThemeMapper.deleteSysThemeById(id);
    }

    @Override
    public List<Ztree> selectTree() {
        List<SysTheme> sysThemes = sysThemeMapper.selectSysThemeList(null);

        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (int i = 0; i < sysThemes.size(); i++) {
            Ztree ztree = new Ztree();
            SysTheme sysTheme = sysThemes.get(i);
            ztree.setId(sysTheme.getId());
            ztree.setpId(0L);
            ztree.setName(sysTheme.getName());
            ztree.setTitle(sysTheme.getName());
            ztrees.add(ztree);

        }
        return ztrees;
    }
}
