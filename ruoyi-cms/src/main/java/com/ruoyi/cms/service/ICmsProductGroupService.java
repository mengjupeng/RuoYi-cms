package com.ruoyi.cms.service;

import java.util.List;
import com.ruoyi.cms.domain.CmsProductGroup;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.cms.domain.CmsMaterial;

/**
 * 产品分组Service接口
 * 
 * @author ruoyi
 * @date 2020-09-12
 */
public interface ICmsProductGroupService 
{
    /**
     * 查询产品分组
     * 
     * @param id 产品分组ID
     * @return 产品分组
     */
    public CmsProductGroup selectCmsProductGroupById(Long id, Long siteId);

    /**
     * 查询产品分组列表
     * 
     * @param cmsProductGroup 产品分组
     * @return 产品分组集合
     */
    public List<CmsProductGroup> selectCmsProductGroupList(CmsProductGroup cmsProductGroup);

    /**
     * 新增产品分组
     * 
     * @param cmsProductGroup 产品分组
     * @return 结果
     */
    public int insertCmsProductGroup(CmsProductGroup cmsProductGroup);

    /**
     * 修改产品分组
     * 
     * @param cmsProductGroup 产品分组
     * @return 结果
     */
    public int updateCmsProductGroup(CmsProductGroup cmsProductGroup);

    /**
     * 批量删除产品分组
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCmsProductGroupByIds(String ids, Long siteId);

    /**
     * 删除产品分组信息
     * 
     * @param id 产品分组ID
     * @return 结果
     */
    public int deleteCmsProductGroupById(Long id, Long siteId);

    /**
     * 查询产品分组树列表
     * 
     * @return 所有产品分组信息
     */
    public List<Ztree> selectCmsProductGroupTree(Long siteId);

}
