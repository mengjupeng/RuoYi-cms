package com.ruoyi.cms.service;

import java.util.List;
import com.ruoyi.cms.domain.CmsMaterialGroup;
import com.ruoyi.common.core.domain.Ztree;

/**
 * 素材分组Service接口
 * 
 * @author ruoyi
 * @date 2020-09-10
 */
public interface ICmsMaterialGroupService 
{
    /**
     * 查询素材分组
     * 
     * @param id 素材分组ID
     * @return 素材分组
     */
    public CmsMaterialGroup selectCmsMaterialGroupById(Long id, Long siteId);

    /**
     * 查询素材分组列表
     * 
     * @param cmsMaterialGroup 素材分组
     * @return 素材分组集合
     */
    public List<CmsMaterialGroup> selectCmsMaterialGroupList(CmsMaterialGroup cmsMaterialGroup);

    /**
     * 新增素材分组
     * 
     * @param cmsMaterialGroup 素材分组
     * @return 结果
     */
    public int insertCmsMaterialGroup(CmsMaterialGroup cmsMaterialGroup);

    /**
     * 修改素材分组
     * 
     * @param cmsMaterialGroup 素材分组
     * @return 结果
     */
    public int updateCmsMaterialGroup(CmsMaterialGroup cmsMaterialGroup);

    /**
     * 批量删除素材分组
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCmsMaterialGroupByIds(String ids, Long siteId);

    /**
     * 删除素材分组信息
     * 
     * @param id 素材分组ID
     * @return 结果
     */
    public int deleteCmsMaterialGroupById(Long id, Long siteId);

    /**
     * 查询素材分组树列表
     * 
     * @return 所有素材分组信息
     */
    public List<Ztree> selectCmsMaterialGroupTree(Long siteId);
}
